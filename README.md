# ELECTRIC VEHICLE SHARING PROBLEM

This repository contains instances of the Electric Vehicle Sharing Problem (EVSP) used in [[1]][id].

## INSTANCE FORMAT

In order to understand the instance file, the lines of the file are described below in order of appearance.

```
❲number of stations❳ ❲number total of vehicles❳
// Next, one line for each station
❲number of vehicles❳ ❲capacity❳ ❲number of charging facilities❳ ... ❲initial energy❳ ❲battery capacity❳ ❲maximum time to charge❳ ❲discharging rate❳
.
.
.
❲number of customers❳
// Next, one line for each customer
❲number of demands❳ ... ❲pick-up station❳ ❲departure time❳ ❲drop-off station❳ ❲arrival time❳ ❲driving time❳
.
.
.
```

The line for each station contains the number of vehicles initially located at the station, the capacity (i.e., the number of parking spaces), the number of charging facilities and the list of vehicles with the initial battery energy of the vehicle, the maximum battery capacity (watt-minutes), the time required to a full charge (in minutes) and the discharging rate of battery (in watts per minute). Note that the energy supplied by a charging facility is defined as ❲battery capacity❳/❲maximum time to charge❳.

The line for each customer contains the number of demands for the customer and their list of demands with the pick-up station, the departure time from the pick-up station, the drop-off station, the arrival time at the drop-off station, and the driving time (in minutes). Note that the energy required on the rental period is defined as ❲driving time❳×❲discharging rate❳.

## REFERENCE

[id]: first_paper
[1] Silva, W.R.; Usberti, F.L.; Schouery, R.C.S. On the complexity and modeling of the electric vehicle sharing problem. [[Preprint](https://arxiv.org/abs/2202.12918)]

